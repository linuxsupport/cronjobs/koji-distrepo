#!/usr/bin/python3
""" Prepare config files"""
import sys
import getopt
import yaml


try:
    opts, args = getopt.getopt(sys.argv[1:], "c:d:", ["config=", "destination="])
except getopt.GetoptError:
    print('prepare.py -c <configfile> -d <destination>')
    sys.exit(1)

for opt, arg in opts:
    if opt in ("-c", "--config"):
        config_name = arg
    elif opt in ("-d", "--destination"):
        destination = arg

with open(config_name, 'r') as configfile:
    config = yaml.safe_load(configfile)

try:
    public_tags = config.get('regex_tag_public', [])
except AttributeError:
    public_tags = []

with open('%s/public.conf' % destination, 'w') as destfile:
    for t in public_tags:
        destfile.write(t + '\n')

try:
    exclude_tags = config.get('regex_tag_excludes', [])
except AttributeError:
    exclude_tags = []

with open('%s/exclude.conf' % destination, 'w') as destfile:
    for t in exclude_tags:
        destfile.write(t + '\n')

try:
    multilib_tags = config.get('regex_tag_multilib', [])
except AttributeError:
    multilib_tags = []

with open('%s/multilibtags.conf' % destination, 'w') as destfile:
    for t in multilib_tags:
        destfile.write(t + '\n')

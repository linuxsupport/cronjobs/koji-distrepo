job "${PREFIX}_koji-distrepo" {
  type = "batch"

  spread {
    attribute = "${node.unique.name}"
  }

  parameterized {
    payload = "required"
    meta_required = ["PARENT_JOB"]
  }

  reschedule {
    attempts       = 132
    interval       = "11h"
    unlimited      = false
    delay          = "5m"
    delay_function = "constant"
  }

  priority = 60

  task "${PREFIX}_koji-distrepo" {
    driver = "docker"

    config {
      image = "https://gitlab-registry.cern.ch/linuxsupport/cronjobs/koji-distrepo/kojidistrepo:${CI_COMMIT_SHORT_SHA}"
      logging {
        config {
          tag = "${PREFIX}_koji-distrepo"
        }
      }
      volumes = [
        "$MP_FINAL:/repo",
        "$MP_STAGING:/staging",
        "$KOJI_PASSWORD:/auth/koji_password",
      ]
    }

    env {
      TAG = "${PREFIX}_koji-distrepo"
    }

    dispatch_payload {
      file = "config.json"
    }

    resources {
      cpu = 5000 # Mhz
      memory = 2048 # MB
    }
  }
}

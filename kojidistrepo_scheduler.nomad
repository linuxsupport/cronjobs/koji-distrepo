job "${PREFIX}_koji-distrepo_scheduler" {
  type = "batch"

  spread {
    attribute = "${node.unique.name}"
  }

  periodic {
    cron = "${SCHEDULE}"
    time_zone = "Europe/Zurich"
    prohibit_overlap = true
  }

  reschedule {
    attempts       = 132
    interval       = "11h"
    unlimited      = false
    delay          = "5m"
    delay_function = "constant"
  }

  task "${PREFIX}_koji-distrepo_scheduler" {
    driver = "docker"

    config {
      image = "https://gitlab-registry.cern.ch/linuxsupport/cronjobs/koji-distrepo/scheduler:${CI_COMMIT_SHORT_SHA}"
      logging {
        config {
          tag = "${PREFIX}_koji-distrepo"
        }
      }
      volumes = [
        "$MP_FINAL:/repo",
        "$MP_STAGING:/staging",
        "/etc/nomad/submit_token:/secrets/token",
        "/etc/nomad/ca.pem:/secrets/nomad-ca.pem"
      ]
    }

    env {
      NOMAD_ADDR = "$NOMAD_ADDR"
      NOMAD_CACERT = "/secrets/nomad-ca.pem"
      KOJIDISTREPO_JOB = "${PREFIX}_koji-distrepo"
      TAG = "${PREFIX}_koji-distrepo"
    }

    resources {
      cpu = 1000 # Mhz
      memory = 128 # MB
    }
  }
}

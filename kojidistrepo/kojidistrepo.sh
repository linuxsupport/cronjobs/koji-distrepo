#!/bin/bash

CONFIG='/local/config.json'
OUTPUT='/alloc/output.json'

REPO_DIR="/repo"
REPOPATH="${REPO_DIR}/internal/repos"
REPOPATH_PUB="${REPO_DIR}/repos"
REPODIST_BUILDS="/staging/buildlists"
COMPS_URL="https://gitlab.cern.ch/linuxsupport/comps.xml/raw/master"

# Create the buildlists directory if it does not exist
mkdir -p ${REPODIST_BUILDS}

log () {
  mapfile IN
  cat << EOF | tr -d '\n'; echo
{ "koji_tag": "${TAG}",
  ${IN[@]}
}
EOF
}

error () {
  mapfile IN
  cat << EOF | log
    "message_type": "error",
    ${IN[@]}
EOF
}

finish () {
  echo "Deleting ${LOCKFILE}"
  rm -f ${LOCKFILE}
  cat << EOF | log
  "message_type": "lock",
  "message": "Lock released"
EOF
}

if [ ! -f $CONFIG ]; then
  RET=-1
  cat << EOF | error | tee $OUTPUT
    "exit_code": $RET,
    "error": "no config file found"
EOF
  exit $RET
fi

# Read the job parameters
TAG="`jq -r '.TAG // ""' $CONFIG`"
ARCHES="`jq -r '.ARCHES // "x86_64"' $CONFIG`"
FORCE="`jq -r '.FORCE // ""' $CONFIG`"

# Replace i686 arch with i386
ARCHES="${ARCHES/i686/i386}"

# Add src
ARCHES+=" src"

cat << EOF | log
  "message_type": "start",
  "message": "Running dist-repo for ${TAG}: ${ARCHES}",
EOF

# See if this tag matches one of the regexp from the list of excluded tags
echo "${TAG}" | grep -q -f /root/exclude.conf && EXCLUDE=0 || EXCLUDE=1
if [[ $EXCLUDE -eq 0 ]]; then
  cat << EOF | log
  "message_type": "result",
  "message": "Nothing to do, excluded tag",
  "exit_code": 0
EOF
  exit 0;
fi

# See if this tag matches one of the regexp from the list of public tags
echo "${TAG}" | grep -q -f /root/public.conf && PUBLIC=1 || PUBLIC=0

# *-testing tags should only build latest packages
echo "${TAG}" | grep -q -e '-testing' && ONLY_LATEST="True" || ONLY_LATEST="False"

# See if this tag matches one of the regexp from the list of multilib tags
echo "${TAG}" | grep -q -f /root/multilibtags.conf && MULTILIB=1 || MULTILIB=0

# Make destination directory if it doesn't exist
[[ ! -d "${REPOPATH}/${TAG}" ]] && mkdir -p ${REPOPATH}/${TAG}


# Locking mechanism:
# We don't want dist-repo to run concurrently, so each tag (except db* and hdp*) gets a lockfile. Koji dist-repo doesn't run unless
# it can aquire the lock. However, it doesn't make sense to have multiple tasks waiting for the same tag, as
# the next one to run will handle all the updates anyway.
#
# This is why we also create a WAITFILE just before aquiring the lock, and delete it just afterwards. If the
# WAITFILE already exists (and it's not stale), it means there's already a koji-distrepo job waiting for that particular task,
# and therefore this task doesn't need to wait as well.

# DB and HDP tags are huge, so don't try to do more than one at a time
if echo "${TAG}" | grep -q -e '^db[0-9]-'; then
  LOCKFILE="${REPOPATH}/db-distrepo.lock"
elif echo "${TAG}" | grep -q -e '^hdp[0-9]-'; then
  LOCKFILE="${REPOPATH}/hdp-distrepo.lock"
else
  LOCKFILE="${REPOPATH}/${TAG}/distrepo.lock"
fi
WAITFILE="${REPOPATH}/${TAG}/distrepo.wait"

if [ -f "${WAITFILE}" ]; then
  MODTIME=`stat -c "%Y" "${WAITFILE}"`
  NOW=`date +%s`
  AGE=$(($NOW - $MODTIME))
  if [[ $AGE -lt 21600 ]]; then
    cat << EOF | log
    "message_type": "lock",
    "message": "Somebody already waiting, we're not needed"
EOF
    exit 0;
  fi
fi
touch "${WAITFILE}"

# Make sure we always release the lock when the script exits
trap finish EXIT

# Grab a lock. Lock is stale if it's more than 6h old
cat << EOF | log
  "message_type": "lock",
  "message": "Grabbing lock"
EOF
lockfile -l 21600 ${LOCKFILE}
rm -f ${WAITFILE}
cat << EOF | log
  "message_type": "lock",
  "message": "Lock acquired"
EOF

# Wait for 10 seconds, just in case multiple builds are being tagged at once.
# This way, hopefully we'll catch them all and not end up creating an inconsistent repo.
# It would be great if there was a better way to do this, maybe there will be some day:
# https://pagure.io/koji/issue/3995
sleep 10s

# Record start time
start=`date +%s`
RUN_DISTREPO=0

# Get list of tagged builds
/usr/bin/koji list-tagged ${TAG} | tail -n +3 | sort > /tmp/${TAG}.buildlist


# Useful variables
TOPURL=`awk -F "=" '/topurl/ {print $2}' /etc/koji.conf | tr -d ' '`
BASE_URL="${TOPURL}/repos-dist/${TAG}/latest/"
SHORTTAG=`echo "${TAG}" | cut -d'-' -f1`
BUILDHOST=`awk -F "=" '/server/ {print $2}' /etc/koji.conf | tr -d ' '`

# Create the .repo file with a tag per arch
for ARCH in $ARCHES; do
  echo "[${TAG}-${ARCH}]" >> /etc/yum.repos.d/${TAG}.repo
  echo "name='${TAG} for ${ARCH}'" >> /etc/yum.repos.d/${TAG}.repo
  echo "baseurl=${BASE_URL}${ARCH}" >> /etc/yum.repos.d/${TAG}.repo
  echo "enabled=1" >> /etc/yum.repos.d/${TAG}.repo
  echo "gpgcheck=0" >> /etc/yum.repos.d/${TAG}.repo
  echo "async=0" >> /etc/yum.repos.d/${TAG}.repo
  echo "" >> /etc/yum.repos.d/${TAG}.repo
done

# Figure out if something has changed or not
if [ ! -f ${REPODIST_BUILDS}/${TAG}.buildlist ]; then
  RUN_DISTREPO=1
else
  NEWBUILDS=`/usr/bin/diff --changed-group-format='%>' --unchanged-group-format='' ${REPODIST_BUILDS}/${TAG}.buildlist /tmp/${TAG}.buildlist`
  OLDBUILDS=`/usr/bin/diff --changed-group-format='%<' --unchanged-group-format='' ${REPODIST_BUILDS}/${TAG}.buildlist /tmp/${TAG}.buildlist`

  if [[ "$NEWBUILDS" != "" || "$OLDBUILDS" != "" || "$FORCE" == "true" ]]; then
    RUN_DISTREPO=1
  fi
fi

# Try to download comps.xml file, fail on 404
curl -fs "${COMPS_URL}/${SHORTTAG}.xml" -o comps.xml
if [[ $? -eq 0 ]]; then
  COMPS="--comps /root/comps.xml"
fi

# Don't do anything if there are no changes
if [[ $RUN_DISTREPO -eq 0 ]]; then
  cat << EOF | log
  "message_type": "result",
  "message": "Nothing to do",
  "exit_code": 0
EOF
  exit 0;
fi

echo "Multilib: ${MULTILIB}"
MULTI_LIB=""
if [[ $MULTILIB -eq 1 ]]; then
  MULTI_LIB="--multilib /root/multilib.conf"
fi

echo "Latest builds: ${ONLY_LATEST}"
LATEST_BUILDS=""
if [[ $ONLY_LATEST == "False" ]]; then
  LATEST_BUILDS="--non-latest"
fi

ARCHS=""
for ARCH in $ARCHES; do
  if [[ $ARCH != "src" ]]; then
    ARCHS+="--arch $ARCH "
  fi
done

if [[ $MULTILIB -eq 1 ]]; then
  ARCHS+="--arch i686 "
fi

# Run dist-repo
echo "Run koji dist-repo  ${TAG} ${ARCHS} --with-src  ${COMPS} ${MULTILIB} ${LATEST_BUILDS} --wait --allow-missing-signatures --noinherit --split-debuginfo"
/usr/bin/koji --user koji --authtype password --password `cat /auth/koji_password` dist-repo ${TAG} \
  ${ARCHS} \
  --with-src \
  ${COMPS} \
  ${MULTI_LIB} \
  ${LATEST_BUILDS} \
  --wait \
  --allow-missing-signatures \
  --noinherit \
  --split-debuginfo
RET=$?

if [[ $RET -ne 0 ]]; then
  cat << EOF | error | tee $OUTPUT
    "exit_code": $RET,
    "error": "dist-repo failed"
EOF
  exit $RET
fi

# Get the repo from kojifiles
for ARCH in $ARCHES; do

  if [[ $ARCH != "src" ]]; then
    REPOSYNC_PATH=${REPOPATH}/${TAG}/${ARCH}/os
  else
    REPOSYNC_PATH=${REPOPATH}/${TAG}/${ARCH}/
  fi

  mkdir -p ${REPOSYNC_PATH}

  # Run reposync
  echo "Run reposync for $TAG $ARCH - in $REPOSYNC_PATH"
  /usr/bin/reposync \
    --delete \
    --remote-time \
    --downloadcomps \
    --download-metadata \
    --norepopath \
    -p ${REPOSYNC_PATH} \
    --repoid ${TAG}-${ARCH}
  RET=$?

  if [[ $RET -ne 0 ]]; then
    cat << EOF | error | tee $OUTPUT
      "exit_code": $RET,
      "error": "reposync error"
EOF
    exit $RET
  fi

  # Remove 0 byte RPMs
  /usr/bin/find ${REPOPATH}/$TAG/$ARCH/ -type f -size 0b -name "*.rpm" -exec rm -v {} \;

  # Move info to /os directory
  if [[ $ARCH != "src" ]]; then
    # Delete empty directories - letter dirs inside of os
    find ${REPOPATH}/$TAG/$ARCH/os -type d -empty -delete

    # Remove debug dir create it from scratch
    rm -rf ${REPOPATH}/$TAG/$ARCH/debug

    # Create a debug directory
    /usr/bin/mkdir ${REPOPATH}/$TAG/$ARCH/debug

    # debug pkglist url
    URL="${BASE_URL}${ARCH}/debug"
    httpreturn=`curl -L "$URL/pkglist" --write-out %{http_code} --silent --output /dev/null`

    pushd "${REPOPATH}/$TAG/$ARCH/debug"

    if [ $httpreturn -eq "200" ]; then
      # Download the package list
      curl --silent -L "$URL/pkglist" -O

      # Prepent the URL to each package
      sed -e "s#^#$URL/#" -i pkglist

      # Download them all
      while read url; do
        echo "Downloading: $url"
        curl -sL -O "$url"
      done < pkglist
      rm pkglist

    else
      echo "Package list not found for tag $TAG and arch $ARCH"
    fi

    createrepo_c -v .

    popd
  fi
done

# Rename src folder
rm -rf ${REPOPATH}/${TAG}/source/
mkdir ${REPOPATH}/${TAG}/source/
mv ${REPOPATH}/${TAG}/src/ ${REPOPATH}/${TAG}/source/SRPMS/

# Create link from the public part, if needed
if [[ $PUBLIC -eq 1 ]]; then
  [[ ! -d "${REPOPATH_PUB}" ]] && mkdir -p ${REPOPATH_PUB}
  ln -sfr ${REPOPATH}/${TAG} ${REPOPATH_PUB}
else
  [ -h ${REPOPATH_PUB}/${TAG} ] && rm ${REPOPATH_PUB}/${TAG}
fi

# Save the new buildlist
mv /tmp/${TAG}.buildlist ${REPODIST_BUILDS}/${TAG}.buildlist

# Set the last run time
date --iso-8601=seconds > ${REPOPATH}/${TAG}/.last_run_time

end=`date +%s`
runtime=$((end-start))


while read -r line; do
  build=`echo $line | /usr/bin/awk '{print $1}'`
  author=`echo $line | /usr/bin/awk '{print $3}'`
  [[ "$build" == "" ]] && break
  cat << EOF | log
    "message_type": "tagged",
    "build": "${build}",
    "author": "${author}"
EOF
done <<< "${NEWBUILDS}"

while read -r line; do
  build=`echo $line | /usr/bin/awk '{print $1}'`
  [[ "$build" == "" ]] && break
  cat << EOF | log
    "message_type": "untagged",
    "build": "${build}"
EOF
done <<< "${OLDBUILDS}"

cat << EOF | log
  "message_type": "result",
  "runtime_secs": ${runtime},
  "exit_code": ${RET}
EOF

#!/usr/bin/python3
"""
Scheduler job get all the tags defined in Koji and trigger a koji distrepo job for each one.
It runs twice a day and the point is to create the repositories for new tags even if no
packages have been added to them yet.
Otherwise users complain because they have a koji tag, they've added it to puppet and
their machines fail because the repo doesn't exist
"""
import re
import sys
import time
import os
import base64
import shutil
import json
import yaml
import koji
import nomad
from nomad.api import exceptions

# Disable SSL warnings
import urllib3
urllib3.disable_warnings()

CONFIG = '/root/config.yaml'
JOBNAME = os.getenv('KOJIDISTREPO_JOB', 'koji-distrepo')
STATUS_CHECK_SECS = os.getenv('STATUS_CHECKS', '5')
REPODIST_BUILDS="/staging/buildlists"
REPOPATH="/repo/internal/repos"

REGEX_TAG_QA = '-qa'
REGEX_TAG_TESTING = '-testing'
REGEX_TAG_STABLE = '-stable'


def log(**kwargs):
    """ Log message"""
    print(json.dumps(kwargs))

def debug(**kwargs):
    """ Debug message"""
    kwargs.update({'message_type': 'debug'})
    log(**kwargs)

def error(**kwargs):
    """ Error message"""
    kwargs.update({'message_type': 'error'})
    log(**kwargs)



try:
    with open('/secrets/token') as f:
        nomad_token = f.read().strip('\n')
except IOError as err:
    error(comment='Unable to read the Nomad token', exception=str(err))
    sys.exit(1)


debug(comment='Connecting to Nomad at: {}'.format(os.getenv('NOMAD_ADDR', None)))
nomads = nomad.Nomad(secure=True, token=nomad_token, timeout=5, verify=False)

try:
    with open(CONFIG) as f:
        config = yaml.load(f.read(), Loader=yaml.FullLoader)
except IOError as e:
    error(comment='Unable to read configuration file {}'.format(CONFIG), exception=str(e))
    sys.exit(1)

# Don't fail even with an empty config file
if not config:
    config = {}

# Set up a koji session
koji_options = koji.read_config('koji')
koji_session = koji.ClientSession(koji_options['server'], koji.grab_session_options(koji_options))

try:
    tags = koji_session.listTags()
except Exception as e:
    error(comment='Unable to connect to Koji: {}'.format(koji_options['server']), exception=str(e))
    sys.exit(1)

try:
    if config['regex_tag_excludes']:
        # Remove excluded tags
        tags = [x for x in tags if not re.search('|'.join(config['regex_tag_excludes']), x['name'])]
except KeyError:
    pass

tags_qa = [x['name'] for x in tags if
           re.search(config.get('regex_tag_qa', REGEX_TAG_QA), x['name'])]
tags_testing = [x['name'] for x in tags if
                re.search(config.get('regex_tag_testing', REGEX_TAG_TESTING), x['name'])]
tags_stable = [x['name'] for x in tags if
               re.search(config.get('regex_tag_stable', REGEX_TAG_STABLE), x['name'])]
tags_all = tags_testing + tags_qa + tags_stable

# Check if there are repos without tags
# Get the repos
listrepos = [repox for repox in os.listdir(REPOPATH) if os.path.isdir(f'{REPOPATH}/{repox}')]

# Repos without tag
koji_tags = [x['name'] for x in tags]
reposnottag = [x for x in listrepos if x not in koji_tags]

# Get the buildlists
listbuildists = [f.split('.')[0] for f in os.listdir(REPODIST_BUILDS)]

# Buildlists without tag
buildnottag = [x for x in listbuildists if x not in koji_tags]

# Current time
current_time = time.time()

# Remove repos and buildlists without tag at least 1 month ago
for misstag in reposnottag:
    modification_time = os.path.getmtime(f"{REPOPATH}/{misstag}")
    diff_time = current_time - modification_time
    diff_time_days = diff_time / 3600 / 24
    log(comment=f"The {misstag} tag was removed/renamed {diff_time_days} days ago but the repo still exists")
    if diff_time_days > 30:
        if misstag.endswith('-todelete'):
            log(comment=f"The {misstag} tag was removed/renamed 2 month ago - time to remove the repo and buildlists")
            shutil.rmtree(f"{REPOPATH}/{misstag}")
            os.remove(f"{REPODIST_BUILDS}/{misstag.split('-todelete')[0]}.buildlist")
        else:
            log(comment=f"The {misstag} tag was removed/renamed 1 month ago - time to rename the repo with -todelete")
            os.rename(f"{REPOPATH}/{misstag}", f"{REPOPATH}/{misstag}-todelete")
            os.utime(f"{REPOPATH}/{misstag}-todelete")

jobs = {}
for tag in tags_all:
    list_tagged = koji_session.listTagged(tag, latest=True)

  # Figure out the arches for this tag. Look at the builds
  # to find the tag they were built under and check that
  # tags arches. We only need the first one, all the others
  # will be the same. We default to 'x86_64 aarch64'.
    arches = 'x86_64 aarch64'
    for build in list_tagged:
        if not build['task_id']:
            continue

        info = koji_session.getTaskInfo(build['task_id'], request=True)
        target = None
        request = info['request']
        if info['method'] in ('build', 'maven'):
            # request is (source-url, build-target, map-of-other-options)
            if request[1]:
                target = koji_session.getBuildTarget(request[1])
        elif info['method'] == 'winbuild':
            # request is (vm-name, source-url, build-target, map-of-other-options)
            if request[2]:
                target = koji_session.getBuildTarget(request[2])
        if not target:
            continue

        target_info = koji_session.getTag(target['build_tag_name'])
        if target_info['arches']:
            arches = target_info['arches']
            break

    payload = {'TAG': tag, 'ARCHES': arches,}
    enc_payload = base64.b64encode(str.encode(json.dumps(payload))).decode()

    try:
        job = nomads.job.dispatch_job(
            JOBNAME, meta={"PARENT_JOB": os.getenv('NOMAD_JOB_NAME', None)},
            payload=enc_payload)
        log(comment='dispatched job', tag=tag, job=job, payload=payload)
        jobs[job['DispatchedJobID']] = tag

    except (exceptions.URLNotFoundNomadException, exceptions.BaseNomadException) as err:
        error(comment='Error dispatching job')
        raise err

log(comment=f'Waiting for {len(jobs)} jobs to finish')
while jobs:
    log(comment=f'Number of jobs to finish: {len(jobs)}')
    dead_jobs = []
    for j in jobs.keys():
        state = nomads.job.get_job(j)
        log(comment='Job state', job=j, tag=jobs[j], state=state['Status'])
        if state['Status'] == 'dead':
            dead_jobs.append(j)

    for dj in dead_jobs:
        log(comment='Deleting job', job=dj)
        del jobs[dj]

    log(comment=f'Sleep time: {STATUS_CHECK_SECS}')
    time.sleep(int(STATUS_CHECK_SECS))

log(comment='All done!')
